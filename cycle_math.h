//
// Created by nikmo on 01.03.2021.
//

#ifndef TASK2_CYCLE_MATH_H
#define TASK2_CYCLE_MATH_H

#include "math.h"



double calculateByMath();
double calculateByCycle();

#endif //TASK2_CYCLE_MATH_H
